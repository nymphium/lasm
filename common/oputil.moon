import Reader, hblockdecode, headassert, chunknamedecode, fnblockdecode from require'luadec.reader'

op_list = require'luadec.op_list'!

-- extractins = (file, fnblock) ->
	-- if file
		-- input = Reader file
		-- header = headassert hblockdecode input
		-- chunknamedecode input, header
		-- fnblock =  fnblockdecode input, header

	-- proto = fnblock.prototype
	-- with fnblock.instruction
		-- .prototype = [extractins nil, fnblock.prototype[i] for i = 1, #proto]

-- pos = {nth depth, mth prototype, lth instruction} 0th depth is main chunk
insertins = (fnblock, pos, ins, nowdepth = 0) ->
	assert (type ins[1]) == (type ins[2]) and (type ins[1]) == "number", "insertins #3: invalid instruction"
	if ins[3] then assert (type ins[3]) == "number", "insertins #3: invalid instruction"
	assert op_list[ins.op], "insertins #3: invalid opcode"

	if pos[1] == 0
		return table.insert fnblock.instruction, pos[3], ins

	if nowdepth == pos[1] - 1
		table.insert fnblock.prototype[pos[2] ].instruction, pos[3], ins
	else
		insertins fnblock.prototype, pos, ins, nowdepth + 1

removeins= (fnblock, pos, nowdepth = 0) ->
	if pos[1] == 0
		return table.remove fnblock.instruction, pos[3]

	if nowdepth == pos[1] - 1
		table.remove fnblock.prototype[pos[2] ].instruction, pos[3]
	else
		removeins fnblock.prototype, pos, nowdepth + 1

swapins = (fnblock, pos, ins) ->
	removeins fnblock, pos
	insertins fnblock, pos, ins

{:insertins, :removeins, :swapins}

