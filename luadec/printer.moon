import undecimal from require'common.utils'

mklbl = (lbl, c = "=") ->
	cc = c\rep 5
	"#{cc}%s#{cc}"\format lbl

typetable = {"nil", "bool", "number", "string", [0x13]: "integer", [0x14]: "long string"}

-- printer
-- {{{
printer = with  head: =>
		for k, v in pairs @
			if type(v) == "table"
				for k_, v_ in pairs v
					print "#{k}.#{k_}", v_
			else
				print k, v

	fnblock = (chunkname, is_closure) => with @
		print "#{is_closure and "function" or "main"} <%s: %d, %d>\n"\format (#chunkname == 0 and '(stripped)' or chunkname), (undecimal.hextoint .line.defined), (undecimal.hextoint .line.lastdefined)
		dbg = .debug
		with ins = .instruction
			print  mklbl"INSTRUCTIONS"
			for i = 1, #@instruction do print ("%-4d  [%d]  %-8s "\format i, dbg.opline[i], ins[i].op), "#{"%-4d "\rep (#ins[i] - 2)}"\format(unpack table.move ins[i], 3, 5, 1, {})

		with cst =  .constant
			print mklbl"CONSTANTS"
			for i = 1, #cst do if l = cst[i] then print "%dth: %7s  %s"\format i, typetable[l.type], l.val

		with pt =  .prototype
			for i = 1, #@prototype
				print mklbl "prototype", "~"
				fnblock pt[i], chunkname, true
				print mklbl "~"\rep(5), "~"

		with .debug
			print mklbl"DEBUG"
			print "\nlocals (#{.varnum})"
			for i = 1, .varnum do print "%-s\t%-d\t%-d"\format unpack .varinfo[i]
			print "\nupvalues (#{.upvnum})"
			for i = 1, .upvnum do print i - 1, "#{.upvname[i]}", @upvalue[i].instack, @upvalue[i].reg

	.fnblock = fnblock
-- }}}

printer

