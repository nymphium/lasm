#!/usr/bin/env moon

import Reader, headassert, hblockdecode, chunknamedecode from require'luadec.reader'
import write, Writer from require'luadec.writer'
import map, filter, foldl, have, delete, last from require'common.utils'
import insert from table
parser = require'lasm.syntax'

header, chunkname = do
	input = Reader string.dump (-> print"hello"), true
	header = headassert hblockdecode input
	chunkname = chunknamedecode input, header
	header, chunkname

newbytecode = {
	header: header
	chunkname: chunkname
	fnblock: {
		prototype: {}
	}
}

format_lasm = (lasms) ->
	for _, proto in pairs lasms
		with proto
			maxreg = 0

			for ins in *.instruction
				if ins.op != ""
					maxreg = math.max maxreg, foldl math.max, 0, ins

				if ins.op == "CLOSURE" and type(ins[2] ) == "string"
					protono = #lasms + 1

					if have lasms, lasms[ins[2] ]
						protono -= 1
					else
						labeled_proto = lasms[ins[2] ]
						lasms[protono] = labeled_proto
						insert newbytecode.fnblock.prototype, labeled_proto

					ins[2] = protono

			-- format constant
			constant = .constant
			if constant then for i = 1, #constant
				cons = constant[i]
				constant[i] = {
						type: switch type cons
							when "number"
								(math.tointeger cons) and 0x13 or 0x03
							when "string"
								(#cons > 255) and 0x14 or 0x04
						val: cons
					}

		-- insert dummy info (debug info, params, etc.)
			.line = {
				defined: "00000000"
				lastdefined: "00000000"
			}

			.regnum = "%02x"\format maxreg
			.params = "00"
			.vararg = "02"
			.upvalue = {
				{
					instack: 1
					reg: 0
				}
			}

			.debug = {
				linenum: 0
				upvnum: 0
				varnum: 0
			}

			.constant or= {}
			.prototype or= {}

	for k in pairs lasms
		-- remove label
		if (type(k) == "string") and  k != "main"
			lasms[k] = nil

	lasms

local lasms
io.close with f = io.open arg[1]
	lasms = format_lasm parser f\read"*a"
	lmain = lasms.main
	with newbytecode.fnblock
		.instruction = lmain.instruction
		.constant = lmain.constant
		.line = lmain.line
		.regnum = lmain.regnum
		.params = lmain.params
		.vararg = lmain.vararg
		.constant = lmain.constant
		.upvalue = lmain.upvalue
		.debug = lmain.debug

wt = Writer "lasm.out"
with newbytecode
	write wt, .header, .chunkname, .fnblock

wt\close!

