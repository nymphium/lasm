# LASM

## syntax
```peg
Lasm      <- Main (Space * Block)*
Main      <- "main" ':' Space Body
Block     <- Label ':' Space Body
Body      <- InsList Space ('{' Space ConstList Space '}')?
InsList   <- Ins (Space Ins)*
Ins       <- ("CLOSURE" Space Number Space Label) / Opcode Space Operand
Opcode    <- [A-Z]+
Operand   <- Number (Space Number){,2}
ConstList <- Const (Space Const)*
Const     <- ('"' ([:print:] - '"')* '"') / Number
Label     <- [a-zA-Z0-9]+
Number    <- '-'? (("0x" [0-9a-f]+) / [0-9]+ ('.' [0-9]+)?)
Space     <- (' ' / '\n' / '\t' / Comment)*
Comment   <- "--" (. - '\n')*
```